import os
import json
import time
import datetime
from tornado import ioloop,web
from functions import specsTalksinPercentage,specsSentimentPercentage,googlePixelLocationWise


class specsTalksinPercentageHandler(web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        data = specsTalksinPercentage()
        if data is None:
            return self.finish(json.dumps({'Status':0,'data':[]}))
        else:
            return self.finish(json.dumps({'Status':1,'data':json.loads(data)}))



class specsSentimentPercentageHandler(web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        data = specsSentimentPercentage
        if data is None:
            return self.finish(json.dumps({'Status':0,'data':[]}))
        else:
            return self.finish(json.dumps({'Status':1,'data':json.loads(data)}))


class googlePixelLocationWiseHandler(web.RequestHandler):
    def get(self):
        self.set_header('Access-Control-Allow-Origin', '*')
        self.set_header('Access-Control-Allow-Credentials', 'true')
        self.set_header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS')
        self.set_header('Access-Control-Allow-Headers','Origin, Accept, Content-Type, X-Requested-With, X-CSRF-Token')
        self.set_header("Content-Type", "application/json")
        self.set_status(200)
        data = googlePixelLocationWise()
        if data is None:
            return self.finish(json.dumps({'Status':0,'data':[]}))
        else:
            return self.finish(json.dumps({'Status':1,'data':json.loads(data)}))


application = web.Application([
    (r'/api/pixel/specs/percent', specsTalksinPercentageHandler),
    (r'/api/pixel/specs/sentiment', specsSentimentPercentageHandler),
    (r'/api/pixel/location', googlePixelLocationWiseHandler),
])

if __name__ == "__main__":
    print "Here we go"
    application.listen(9909)
    ioloop.IOLoop.instance().start()
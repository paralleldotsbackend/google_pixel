import os
import json
import time
import datetime
from tornado import ioloop,web
import redis
import operator
from pymongo import MongoClient
from collections import Counter
import operator

twitter_db = MongoClient("mongodb://104.155.210.134/GnipDataFinal",27017)["GnipDataFinal"]["tweets"]
last_date  = "2016-10-17"
match_date = datetime.datetime.strptime(str(last_date),"%Y-%m-%d")

# def iphoneTweetsStats():
#     get_battery_data     = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "battery", "$options": 'si' } }] }]})
#     battery_tweets_count = get_battery_data.count()

#     get_os_data          = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "android 7.0", "$options": 'si' } }, { "tweet.text": { "$regex": "nougat", "$options": 'si' } }, { "tweet.text": { "$regex": " os ", "$options": 'si' } } ]}]})
#     os_tweets_count      = get_os_data.count()

#     get_camera_data      = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "camera", "$options": 'si' } }, { "tweet.text": { "$regex": " mp ", "$options": 'si' } }, { "tweet.text": { "$regex": "mega pixel", "$options": 'si' } }, { "tweet.text": { "$regex": "dual led flash", "$options": 'si' } }, { "tweet.text": { "$regex": "videos", "$options": 'si' } }, { "tweet.text": { "$regex": "ultra hd", "$options": 'si' }}, {"tweet.text": {"$regex": "picture", "$options": 'si'}},{"tweet.text": { "$regex": "pictures", "$options": 'si'}},{"tweet.text": { "$regex": "slow motion", "$options": 'si'}},{"tweet.text": { "$regex": "slow mo", "$options": 'si'}},{"tweet.text": { "$regex": "slo mo", "$options": 'si'}} ]}]})
#     camera_tweets_count  = get_camera_data.count()
    
#     get_design_data       = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "sound", "$options": 'si' } }, { "tweet.text": { "$regex": "head phone jack", "$options": 'si' } }, { "tweet.text": { "$regex": "loudspeaker", "$options": 'si' } }, { "tweet.text": { "$regex": "headphones", "$options": 'si' } }, { "tweet.text": { "$regex": "earphone jack", "$options": 'si' } }, { "tweet.text": { "$regex": "design", "$options": 'si' } } ] }]})
#     design_tweets_count   = get_design_data.count()
    
#     get_price_data       = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "price", "$options": 'si' } }, { "tweet.text": { "$regex": "cost", "$options": 'si' } } ]}]})
#     price_tweets_count   = get_price_data.count()
    
#     max_tweets = battery_tweets_count+os_tweets_count+camera_tweets_count+design_tweets_count+price_tweets_count

#     price = []
#     price_sentiment = 0.0

#     battery = []
#     b_sentiment = 0.0
    
#     os = []
#     os_sentiment = 0.0

#     camera = []
#     camera_sentiment = 0.0

#     design = []
#     design_sentiment = 0.0

#     battery_location = []
#     os_location = []
#     camera_location = []
#     design_location = []
#     price_location = []

#     battery_p_sentiment = 0
#     battery_n_sentiment = 0
#     battery_neutral_sentiment = 0

#     os_p_sentiment = 0
#     os_n_sentiment = 0
#     os_neutral_sentiment = 0

#     camera_p_sentiment = 0
#     camera_n_sentiment = 0
#     camera_neutral_sentiment = 0

#     design_p_sentiment = 0
#     design_n_sentiment = 0
#     design_neutral_sentiment = 0

#     price_p_sentiment = 0
#     price_n_sentiment = 0
#     price_neutral_sentiment = 0
    
#     for obj in get_battery_data:
#         try:
#             b_sentiment += obj.get('sentiment')
            
#             if obj.get('sentiment') > 0.7:
#                 battery_p_sentiment += 1
#             elif obj.get('sentiment') < 0.3:
#                 battery_n_sentiment += 1
#             else:
#                 battery_neutral_sentiment += 1
            
#             try:
#                 if obj.get('location').get('country') is not None:
#                     battery_location.append(str(obj.get('location').get('country')))
#             except Exception, e:
#                 print e
            
#             battery.append({ 'twid':obj.get('tweet').get('id'),'user_name':obj.get('tweet').get('user').get('name'), 'profile_link': obj.get('tweet').get('user').get('image'), 'location':obj.get('tweet').get('user').get('location'), 'userid':obj.get('tweet').get('user').get('id'),"screen_name":obj.get('tweet').get('user').get('screen_name'),"count":obj.get('tweet').get('retweet_count')+obj.get('tweet').get('favorite_count'), "text":obj.get('tweet').get('text'),"twitter_link": "twitter.com/"+str(obj.get('tweet').get('user').get('id'))+"/status/"+str(obj.get('tweet').get('id'))})
#         except Exception, e:
#             print e
#             continue

#     b_sentiment = b_sentiment/battery_tweets_count
    
#     if battery_p_sentiment > battery_n_sentiment and battery_p_sentiment > battery_neutral_sentiment:
#         b_sentiment_status = "Positive"
#     elif battery_n_sentiment > battery_p_sentiment and battery_n_sentiment > battery_neutral_sentiment:
#         b_sentiment_status = "Negative"
#     else:
#         b_sentiment_status = "Neutral"
    
#     battery = sorted(battery, key=operator.itemgetter('count'),reverse=True)[:10]
#     c = Counter(battery_location).most_common(5)
#     battery_top_location = ""
#     for i in range(0,len(c)):
#         if c[i][0] is None:
#             continue
#         else:
#             battery_top_location = str(c[i][0])
#             break
    

#     for obj in get_os_data:
#         try:
#             os_sentiment += obj.get('sentiment')
#             if obj.get('sentiment') > 0.7:
#                 os_p_sentiment += 1
#             elif obj.get('sentiment') < 0.3:
#                 os_n_sentiment += 1
#             else:
#                 os_neutral_sentiment += 1
            
#             try:
#                 if obj.get('location').get('country') is not None:
#                     os_location.append(str(obj.get('location').get('country')))
#             except Exception, e:
#                 print e

#             os.append({ 'twid':obj.get('tweet').get('id'),'user_name':obj.get('tweet').get('user').get('name'), 'profile_link': obj.get('tweet').get('user').get('image'), 'location':obj.get('tweet').get('user').get('location'), 'userid':obj.get('tweet').get('user').get('id'),"screen_name":obj.get('tweet').get('user').get('screen_name'),"count":obj.get('tweet').get('retweet_count')+obj.get('tweet').get('favorite_count'), "text":obj.get('tweet').get('text'),"twitter_link": "twitter.com/"+str(obj.get('tweet').get('user').get('id'))+"/status/"+str(obj.get('tweet').get('id'))})
#         except Exception, e:
#             print e
#             continue    
    
#     os_sentiment = os_sentiment/os_tweets_count*1.0
    
#     if os_p_sentiment > os_n_sentiment and os_p_sentiment > os_neutral_sentiment:
#         os_sentiment_status = "Positive"
#     elif os_n_sentiment > os_p_sentiment and os_n_sentiment > os_neutral_sentiment:
#         os_sentiment_status = "Negative"
#     else:
#         os_sentiment_status = "Neutral"

#     os = sorted(os, key=operator.itemgetter('count'),reverse=True)[:10]
    
#     c = Counter(os_location).most_common(5)
#     os_top_location = ""
#     for i in range(0,len(c)):
#         if c[i][0] is None:
#             continue
#         else:
#             os_top_location = str(c[i][0])
#             break

    
#     for obj in get_camera_data:
#         try:
#             camera_sentiment += obj.get('sentiment')

#             if obj.get('sentiment') > 0.7:
#                 camera_p_sentiment += 1
#             elif obj.get('sentiment') < 0.3:
#                 camera_n_sentiment += 1
#             else:
#                 camera_neutral_sentiment += 1
#             try:
#                 if obj.get('location').get('country') is not None:
#                     camera_location.append(obj.get('location').get('country'))
#             except Exception, e:
#                 print e
#             camera.append({ 'twid':obj.get('tweet').get('id'),'user_name':obj.get('tweet').get('user').get('name'), 'profile_link': obj.get('tweet').get('user').get('image'), 'location':obj.get('tweet').get('user').get('location'), 'userid':obj.get('tweet').get('user').get('id'),"screen_name":obj.get('tweet').get('user').get('screen_name'),"count":obj.get('tweet').get('retweet_count')+obj.get('tweet').get('favorite_count'), "text":obj.get('tweet').get('text'),"twitter_link": "twitter.com/"+str(obj.get('tweet').get('user').get('id'))+"/status/"+str(obj.get('tweet').get('id'))})
#         except Exception, e:
#             print e
#             continue

#     camera_sentiment = camera_sentiment/camera_tweets_count
    
#     if camera_p_sentiment > camera_n_sentiment and camera_p_sentiment > camera_neutral_sentiment:
#         camera_sentiment_status = "Positive"
#     elif camera_n_sentiment > camera_p_sentiment and camera_n_sentiment > camera_neutral_sentiment:
#         camera_sentiment_status = "Negative"
#     else:
#         camera_sentiment_status = "Neutral"

#     camera = sorted(camera, key=operator.itemgetter('count'),reverse=True)[:10]

#     c = Counter(camera_location).most_common(5)
#     camera_top_location = ""
#     for i in range(0,len(c)):
#         if c[i][0] is None:
#             continue
#         else:
#             camera_top_location = str(c[i][0])
#             break
    
#     for obj in get_design_data:
#         try:
#             design_sentiment += obj.get('sentiment')
#             if obj.get('sentiment') > 0.7:
#                 design_p_sentiment += 1
#             elif obj.get('sentiment') < 0.3:
#                 design_n_sentiment += 1
#             else:
#                 design_neutral_sentiment += 1
#             try:
#                 if obj.get('location').get('country') is not None:
#                     design_location.append(str(obj.get('location').get('country')))
#             except Exception, e:
#                 print e
            
#             design.append({ 'twid':obj.get('tweet').get('id'),'user_name':obj.get('tweet').get('user').get('name'), 'profile_link': obj.get('tweet').get('user').get('image'), 'location':obj.get('tweet').get('user').get('location'), 'userid':obj.get('tweet').get('user').get('id'),"screen_name":obj.get('tweet').get('user').get('screen_name'),"count":obj.get('tweet').get('retweet_count')+obj.get('tweet').get('favorite_count'), "text":obj.get('tweet').get('text'),"twitter_link": "twitter.com/"+str(obj.get('tweet').get('user').get('id'))+"/status/"+str(obj.get('tweet').get('id'))})
#         except Exception, e:
#             print e
#             continue
            

#     design_sentiment = design_sentiment/design_tweets_count
    
#     if design_p_sentiment > design_n_sentiment and design_p_sentiment > design_neutral_sentiment:
#         design_sentiment_status = "Positive"
#     elif design_n_sentiment > design_p_sentiment and design_n_sentiment > design_neutral_sentiment:
#         design_sentiment_status = "Negative"
#     else:
#         design_sentiment_status = "Neutral"

#     design = sorted(design, key=operator.itemgetter('count'),reverse=True)[:10]
    

#     c = Counter(design_location).most_common(5)
#     design_top_location = ""
#     for i in range(0,len(c)):
#         if c[i][0] is None:
#             continue
#         else:
#             design_top_location = str(c[i][0])
#             break
    
#     for obj in get_price_data:
#         try:
#             price_sentiment += obj.get('sentiment')
#             if obj.get('sentiment') > 0.7:
#                 price_p_sentiment += 1
#             elif obj.get('sentiment') < 0.3:
#                 price_n_sentiment += 1
#             else:
#                 price_neutral_sentiment += 1
            
#             try:
#                 if obj.get('location').get('country') is not None:
#                     price_location.append(str(obj.get('location').get('country')))
#             except Exception, e:
#                 print e
            
#             price.append({ 'twid':obj.get('tweet').get('id'),'user_name':obj.get('tweet').get('user').get('name'), 'profile_link': obj.get('tweet').get('user').get('image'), 'location':obj.get('tweet').get('user').get('location'), 'userid':obj.get('tweet').get('user').get('id'),"screen_name":obj.get('tweet').get('user').get('screen_name'),"count":obj.get('tweet').get('retweet_count')+obj.get('tweet').get('favorite_count'), "text":obj.get('tweet').get('text'),"twitter_link": "twitter.com/"+str(obj.get('tweet').get('user').get('id'))+"/status/"+str(obj.get('tweet').get('id'))})
#         except Exception, e:
#             print e
#             continue
    
#     price_sentiment = price_sentiment/price_tweets_count    
    
#     if price_p_sentiment > price_n_sentiment and price_p_sentiment > price_neutral_sentiment:
#         price_sentiment_status = "Positive"
#     elif price_n_sentiment > price_p_sentiment and price_n_sentiment > price_neutral_sentiment:
#         price_sentiment_status = "Negative"
#     else:
#         price_sentiment_status = "Neutral"

#     price = sorted(price, key=operator.itemgetter('count'),reverse=True)[:10]
    
#     c = Counter(price_location).most_common(5)
#     price_top_location = ""
#     for i in range(0,len(c)):
#         if c[i][0] is None:
#             continue
#         else:
#             price_top_location = str(c[i][0])
#             break

#     return [{'feature':"Battery", 'sentiment':b_sentiment_status,'tweets':battery,'count':(battery_tweets_count*100.0)/max_tweets,'top_location':battery_top_location,'positive_sentiment':(battery_p_sentiment*100.0)/battery_tweets_count,'negative_sentiment':(battery_n_sentiment*100.0)/battery_tweets_count,'neutral_sentiment':(battery_neutral_sentiment*100.0)/battery_tweets_count}, {'feature':"OS",'sentiment':os_sentiment_status,'tweets':os,'count':(os_tweets_count*100.0)/max_tweets,'top_location':os_top_location,'positive_sentiment':(os_p_sentiment*100.0)/os_tweets_count,'negative_sentiment':(os_n_sentiment*100.0)/os_tweets_count,'neutral_sentiment':(os_neutral_sentiment*100.0)/os_tweets_count}, {'feature':'Camera','sentiment':camera_sentiment_status,'tweets':camera,'count':(camera_tweets_count*100.0)/max_tweets,'top_location':camera_top_location,'positive_sentiment':(camera_p_sentiment*100.0)/camera_tweets_count,'negative_sentiment':(camera_n_sentiment*100.0)/camera_tweets_count,'neutral_sentiment':(camera_neutral_sentiment*100.0)/camera_tweets_count}, {'feature':"Design",'sentiment':design_sentiment_status,'tweets':design,'count':(design_tweets_count*100.0)/max_tweets,'top_location':design_top_location,'positive_sentiment':(design_p_sentiment*100.0)/design_tweets_count,'negative_sentiment':(design_n_sentiment*100.0)/design_tweets_count,'neutral_sentiment':(design_neutral_sentiment*100.0)/design_tweets_count},{'feature':"Price", 'sentiment':price_sentiment_status,'tweets':price,'count':(price_tweets_count*100.0)/max_tweets,'top_location':price_top_location,'positive_sentiment':(price_p_sentiment*100.0)/price_tweets_count,'negative_sentiment':(price_n_sentiment*100.0)/price_tweets_count,'neutral_sentiment':(price_neutral_sentiment*100.0)/price_tweets_count }]


def specsTalksinPercentage():
    get_battery_data     = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "battery", "$options": 'si' } }] }]})
    battery_tweets_count = get_battery_data.count()

    get_os_data          = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "android 7.0", "$options": 'si' } }, { "tweet.text": { "$regex": "nougat", "$options": 'si' } }, { "tweet.text": { "$regex": " os ", "$options": 'si' } } ]}]})
    os_tweets_count      = get_os_data.count()

    get_camera_data      = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "camera", "$options": 'si' } }, { "tweet.text": { "$regex": " mp ", "$options": 'si' } }, { "tweet.text": { "$regex": "mega pixel", "$options": 'si' } }, { "tweet.text": { "$regex": "dual led flash", "$options": 'si' } }, { "tweet.text": { "$regex": "videos", "$options": 'si' } }, { "tweet.text": { "$regex": "ultra hd", "$options": 'si' }}, {"tweet.text": {"$regex": "picture", "$options": 'si'}},{"tweet.text": { "$regex": "pictures", "$options": 'si'}},{"tweet.text": { "$regex": "slow motion", "$options": 'si'}},{"tweet.text": { "$regex": "slow mo", "$options": 'si'}},{"tweet.text": { "$regex": "slo mo", "$options": 'si'}} ]}]})
    camera_tweets_count  = get_camera_data.count()
    
    get_design_data      = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "sound", "$options": 'si' } }, { "tweet.text": { "$regex": "head phone jack", "$options": 'si' } }, { "tweet.text": { "$regex": "loudspeaker", "$options": 'si' } }, { "tweet.text": { "$regex": "headphones", "$options": 'si' } }, { "tweet.text": { "$regex": "earphone jack", "$options": 'si' } }, { "tweet.text": { "$regex": "design", "$options": 'si' } } ] }]})
    design_tweets_count  = get_design_data.count()
    
    max_tweets           = battery_tweets_count+os_tweets_count+camera_tweets_count+design_tweets_count

    battery_percentage   = (battery_tweets_count*1.0/max_tweets*1.0)*100.0
    os_percentage        = (os_tweets_count*1.0/max_tweets*1.0)*100.0 
    camera_percentage    = (camera_tweets_count*1.0/max_tweets*1.0)*100.0
    design_percentage    = (design_tweets_count*1.0/max_tweets*1.0)*100.0

    return {'battery':battery_percentage,'os':os_percentage,'camera':camera_percentage,'design':design_percentage}


def specsSentimentPercentage():
    get_battery_data     = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "battery", "$options": 'si' } }] }]})
    battery_tweets_count = get_battery_data.count()

    get_os_data          = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "android 7.0", "$options": 'si' } }, { "tweet.text": { "$regex": "nougat", "$options": 'si' } }, { "tweet.text": { "$regex": " os ", "$options": 'si' } } ]}]})
    os_tweets_count      = get_os_data.count()

    get_camera_data      = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "camera", "$options": 'si' } }, { "tweet.text": { "$regex": " mp ", "$options": 'si' } }, { "tweet.text": { "$regex": "mega pixel", "$options": 'si' } }, { "tweet.text": { "$regex": "dual led flash", "$options": 'si' } }, { "tweet.text": { "$regex": "videos", "$options": 'si' } }, { "tweet.text": { "$regex": "ultra hd", "$options": 'si' }}, {"tweet.text": {"$regex": "picture", "$options": 'si'}},{"tweet.text": { "$regex": "pictures", "$options": 'si'}},{"tweet.text": { "$regex": "slow motion", "$options": 'si'}},{"tweet.text": { "$regex": "slow mo", "$options": 'si'}},{"tweet.text": { "$regex": "slo mo", "$options": 'si'}} ]}]})
    camera_tweets_count  = get_camera_data.count()
    
    get_design_data      = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "sound", "$options": 'si' } }, { "tweet.text": { "$regex": "head phone jack", "$options": 'si' } }, { "tweet.text": { "$regex": "loudspeaker", "$options": 'si' } }, { "tweet.text": { "$regex": "headphones", "$options": 'si' } }, { "tweet.text": { "$regex": "earphone jack", "$options": 'si' } }, { "tweet.text": { "$regex": "design", "$options": 'si' } } ] }]})
    design_tweets_count  = get_design_data.count()
    
    get_price_data       = twitter_db.find({"$and":[{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}},{ "$or": [ { "tweet.text": { "$regex": "price", "$options": 'si' } }, { "tweet.text": { "$regex": "cost", "$options": 'si' } } ]}]})
    price_tweets_count   = get_price_data.count()
    
    max_tweets           = battery_tweets_count+os_tweets_count+camera_tweets_count+design_tweets_count+price_tweets_count


    price = []
    price_sentiment = 0.0

    battery = []
    b_sentiment = 0.0
    
    os = []
    os_sentiment = 0.0

    camera = []
    camera_sentiment = 0.0

    design = []
    design_sentiment = 0.0


    battery_p_sentiment = 0
    battery_n_sentiment = 0
    battery_neutral_sentiment = 0

    os_p_sentiment = 0
    os_n_sentiment = 0
    os_neutral_sentiment = 0

    camera_p_sentiment = 0
    camera_n_sentiment = 0
    camera_neutral_sentiment = 0

    design_p_sentiment = 0
    design_n_sentiment = 0
    design_neutral_sentiment = 0

    price_p_sentiment = 0
    price_n_sentiment = 0
    price_neutral_sentiment = 0
    

    for obj in get_battery_data:
        try:
            b_sentiment += obj.get('sentiment')
            
            if obj.get('sentiment') > 0.7:
                battery_p_sentiment += 1
            elif obj.get('sentiment') < 0.3:
                battery_n_sentiment += 1
            else:
                battery_neutral_sentiment += 1
            
            battery.append({ 'twid':obj.get('tweet').get('id'),'user_name':obj.get('tweet').get('user').get('name'), 'profile_link': obj.get('tweet').get('user').get('image'), 'location':obj.get('tweet').get('user').get('location'), 'userid':obj.get('tweet').get('user').get('id'),"screen_name":obj.get('tweet').get('user').get('screen_name'),"count":obj.get('tweet').get('retweet_count')+obj.get('tweet').get('favorite_count'), "text":obj.get('tweet').get('text'),"twitter_link": "twitter.com/"+str(obj.get('tweet').get('user').get('id'))+"/status/"+str(obj.get('tweet').get('id'))})
        except Exception, e:
            print e
            continue
    
    battery = sorted(battery, key=operator.itemgetter('count'),reverse=True)[:5]



    for obj in get_os_data:
        try:
            os_sentiment += obj.get('sentiment')
            if obj.get('sentiment') > 0.7:
                os_p_sentiment += 1
            elif obj.get('sentiment') < 0.3:
                os_n_sentiment += 1
            else:
                os_neutral_sentiment += 1
            
            os.append({ 'twid':obj.get('tweet').get('id'),'user_name':obj.get('tweet').get('user').get('name'), 'profile_link': obj.get('tweet').get('user').get('image'), 'location':obj.get('tweet').get('user').get('location'), 'userid':obj.get('tweet').get('user').get('id'),"screen_name":obj.get('tweet').get('user').get('screen_name'),"count":obj.get('tweet').get('retweet_count')+obj.get('tweet').get('favorite_count'), "text":obj.get('tweet').get('text'),"twitter_link": "twitter.com/"+str(obj.get('tweet').get('user').get('id'))+"/status/"+str(obj.get('tweet').get('id'))})
        except Exception, e:
            print e
            continue    
    
    os_sentiment = os_sentiment/os_tweets_count*1.0
    
    os = sorted(os, key=operator.itemgetter('count'),reverse=True)[:10]
    
    for obj in get_camera_data:
        try:
            camera_sentiment += obj.get('sentiment')

            if obj.get('sentiment') > 0.7:
                camera_p_sentiment += 1
            elif obj.get('sentiment') < 0.3:
                camera_n_sentiment += 1
            else:
                camera_neutral_sentiment += 1
            
            camera.append({ 'twid':obj.get('tweet').get('id'),'user_name':obj.get('tweet').get('user').get('name'), 'profile_link': obj.get('tweet').get('user').get('image'), 'location':obj.get('tweet').get('user').get('location'), 'userid':obj.get('tweet').get('user').get('id'),"screen_name":obj.get('tweet').get('user').get('screen_name'),"count":obj.get('tweet').get('retweet_count')+obj.get('tweet').get('favorite_count'), "text":obj.get('tweet').get('text'),"twitter_link": "twitter.com/"+str(obj.get('tweet').get('user').get('id'))+"/status/"+str(obj.get('tweet').get('id'))})
        except Exception, e:
            print e
            continue
    
    camera = sorted(camera, key=operator.itemgetter('count'),reverse=True)[:10]

    
    for obj in get_design_data:
        try:
            design_sentiment += obj.get('sentiment')
            if obj.get('sentiment') > 0.7:
                design_p_sentiment += 1
            elif obj.get('sentiment') < 0.3:
                design_n_sentiment += 1
            else:
                design_neutral_sentiment += 1
            
            design.append({ 'twid':obj.get('tweet').get('id'),'user_name':obj.get('tweet').get('user').get('name'), 'profile_link': obj.get('tweet').get('user').get('image'), 'location':obj.get('tweet').get('user').get('location'), 'userid':obj.get('tweet').get('user').get('id'),"screen_name":obj.get('tweet').get('user').get('screen_name'),"count":obj.get('tweet').get('retweet_count')+obj.get('tweet').get('favorite_count'), "text":obj.get('tweet').get('text'),"twitter_link": "twitter.com/"+str(obj.get('tweet').get('user').get('id'))+"/status/"+str(obj.get('tweet').get('id'))})
        except Exception, e:
            print e
            continue
            

    design = sorted(design, key=operator.itemgetter('count'),reverse=True)[:10]
    
    for obj in get_price_data:
        try:
            price_sentiment += obj.get('sentiment')
            if obj.get('sentiment') > 0.7:
                price_p_sentiment += 1
            elif obj.get('sentiment') < 0.3:
                price_n_sentiment += 1
            else:
                price_neutral_sentiment += 1
            
            price.append({ 'twid':obj.get('tweet').get('id'),'user_name':obj.get('tweet').get('user').get('name'), 'profile_link': obj.get('tweet').get('user').get('image'), 'location':obj.get('tweet').get('user').get('location'), 'userid':obj.get('tweet').get('user').get('id'),"screen_name":obj.get('tweet').get('user').get('screen_name'),"count":obj.get('tweet').get('retweet_count')+obj.get('tweet').get('favorite_count'), "text":obj.get('tweet').get('text'),"twitter_link": "twitter.com/"+str(obj.get('tweet').get('user').get('id'))+"/status/"+str(obj.get('tweet').get('id'))})
        except Exception, e:
            print e
            continue
    
    price = sorted(price, key=operator.itemgetter('count'),reverse=True)[:10]
    
    return [{'feature':"Battery",'tweets':battery,'positive_sentiment':(battery_p_sentiment*100.0)/battery_tweets_count,'negative_sentiment':(battery_n_sentiment*100.0)/battery_tweets_count,'neutral_sentiment':(battery_neutral_sentiment*100.0)/battery_tweets_count}, {'feature':"OS",'tweets':os,'positive_sentiment':(os_p_sentiment*100.0)/os_tweets_count,'negative_sentiment':(os_n_sentiment*100.0)/os_tweets_count,'neutral_sentiment':(os_neutral_sentiment*100.0)/os_tweets_count}, {'feature':'Camera','tweets':camera,'positive_sentiment':(camera_p_sentiment*100.0)/camera_tweets_count,'negative_sentiment':(camera_n_sentiment*100.0)/camera_tweets_count,'neutral_sentiment':(camera_neutral_sentiment*100.0)/camera_tweets_count}, {'feature':"Design",'tweets':design,'positive_sentiment':(design_p_sentiment*100.0)/design_tweets_count,'negative_sentiment':(design_n_sentiment*100.0)/design_tweets_count,'neutral_sentiment':(design_neutral_sentiment*100.0)/design_tweets_count},{'feature':"Price",'tweets':price,'positive_sentiment':(price_p_sentiment*100.0)/price_tweets_count,'negative_sentiment':(price_n_sentiment*100.0)/price_tweets_count,'neutral_sentiment':(price_neutral_sentiment*100.0)/price_tweets_count }]
    


def googlePixelLocationWise():
    data = twitter_db.aggregate([{"$match":{"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}}},{"$group" : {'_id':"$location.country", 'count':{'$sum':1}}},{ '$sort' :{'count' :-1}},{ '$limit' : 21 }])
    temp = []
    get_tweets_data     = twitter_db.find({"tagname":"Google Pixel keyword",'tweet.timestamp':{'$gte':match_date}})
    location_list = []
    for obj in get_tweets_data:
        location_list.append({'location':obj.get('location').get('country'),'sentiment':obj.get('sentiment')})

    for location in data:
        if location.get('_id') is None:
            continue
        else:
            price = []
            price_sentiment = 0.0

            price_p_sentiment = 0
            price_n_sentiment = 0
            price_neutral_sentiment = 0
            
            for obj in location_list:
                try:
                    if obj.get('location') == location.get('_id'):
                        if obj.get('sentiment') > 0.7:
                            price_p_sentiment += 1
                        elif obj.get('sentiment') < 0.3:
                            price_n_sentiment += 1
                        else:
                            price_neutral_sentiment += 1
                    else:
                        continue        
                    
                except Exception, e:
                    print e
                    continue
            
            print "====================================="
            print price_p_sentiment
            print price_n_sentiment
            print price_neutral_sentiment
            print "====================================="
            
            if price_p_sentiment > price_n_sentiment and price_p_sentiment > price_neutral_sentiment:
                price_sentiment_status = "Positive"
            elif price_n_sentiment > price_p_sentiment and price_n_sentiment > price_neutral_sentiment:
                price_sentiment_status = "Negative"
            else:
                price_sentiment_status = "Neutral"

            temp.append({'location':location.get('_id'),'stats':"Google Pixel",'sentiment':price_sentiment_status})

    return temp










# print "========================="
# print iphoneTweetsStats()
# print "========================="

# print googlePixelLocationWise()

print googlePixelLocationWise()